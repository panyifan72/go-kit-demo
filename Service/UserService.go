package Service

import (
	"errors"
)

type UserInterface interface {
	GetUserName(userId int) string
	DeleteUserById(userId int) error
}

type UserService struct {
	
}

func (this 	UserService)  GetUserName(userId int) string{
	if userId == 101 {
		return "go"
	}

	return "guest"
}

func (this UserService) DeleteUserById(userId int) error{
	if userId == 101 {
		return nil
	}

	return errors.New("wrong")
}
