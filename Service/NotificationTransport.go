package Service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/consul"
	"github.com/go-kit/kit/sd/lb"
	transportHttp "github.com/go-kit/kit/transport/http"
	"io"
	"go-kit-demo/Util"
	"net/http"
	"net/url"
	"os"
)

const TAGS = "first system"
const SERVER = "test-kit-notification"

func SendSmsRequest(_ context.Context, request *http.Request, r interface{}) error {
	smsRequest := r.(NotificationSmsRequest)
	request.URL.Path += "/sms/" + smsRequest.Phone

	return nil
}

func SendSmsResponse(_ context.Context, res *http.Response) (response interface{}, err error) {
	if res.StatusCode != 200 {
		return nil, errors.New("something wrong")
	}

	var SmsResponse NotificationSmsResponse
	err = json.NewDecoder(res.Body).Decode(&SmsResponse)
	if err != nil {
		return nil, err
	}

	return SmsResponse, nil
}

func GetNotificationUrl() (endpoint.Endpoint, error) {
	var logger log.Logger

	logger = log.NewLogfmtLogger(os.Stdout)

	tags := []string{TAGS}
	instancer := consul.NewInstancer(consul.NewClient(Util.ConsulClient), logger, SERVER, tags, true)

	factory := func(instance string) (endpoint.Endpoint, io.Closer, error) {
		tart, _ := url.Parse("http://" + instance)

		return transportHttp.NewClient("POST", tart, SendSmsRequest, SendSmsResponse).Endpoint(), nil, nil
	}

	endPointer := sd.NewEndpointer(instancer, factory, logger)
	//获取负载均衡的
	mylb := lb.NewRoundRobin(endPointer)

	return mylb.Endpoint()
}

func SendSms(phone string) (string, error) {
	endPoint, err := GetNotificationUrl()
	if err != nil {
		return "", err
	}

	ctx := context.Background()
	res, err := endPoint(ctx, NotificationSmsRequest{Phone: phone})
	if err != nil {
		return "", err
	}

	smsResponse := res.(NotificationSmsResponse)

	return smsResponse.Message, nil
}

func DoNotificationSendSms(phone string) {
	//
	configA := hystrix.CommandConfig{
		Timeout:                Util.Timeout,
		MaxConcurrentRequests:  Util.MaxConcurrentRequests,
		RequestVolumeThreshold: Util.RequestVolumeThreshold,
		//SleepWindow:            int(time.Second * Util.SleepWindow),
		ErrorPercentThreshold:  Util.ErrorPercentThreshold,
	}

	hystrix.ConfigureCommand(Util.NOTIF_CONFIG_NAME, configA)
	err := hystrix.Do(Util.NOTIF_CONFIG_NAME, func() error {
		_, err := SendSms(phone)

		return err
	}, func(e error) error {
		fmt.Println("降级用户")
		return e
	})
	if err != nil {
		fmt.Println(err)
	}
}
