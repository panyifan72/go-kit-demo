package Service

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"go-kit-demo/Util"
	"net/http"
	"strconv"
)

func DecodeUserRequest(c context.Context, r *http.Request) (interface{}, error) {
	params := mux.Vars(r)
	if uid, ok := params["uid"]; ok {
		uid, _ := strconv.Atoi(uid)

		return UserRequest{Uid: uid, Method: r.Method}, nil
	}

	return nil, errors.New("参数错误")
}

func EncodeUserRequest(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("content-type", "application/json")
	return json.NewEncoder(w).Encode(response)
}

func UserErrorEncoder(ctx context.Context, err error, w http.ResponseWriter) {
	contentType, body := "text/plain; charset=utf-8", []byte(err.Error())
	w.Header().Set("Content-type", contentType) //设置请求头
	if userError, ok := err.(*Util.UserError); ok {
		w.WriteHeader(userError.Code)
		w.Write(body)
	} else {
		w.WriteHeader(500) //写入返回码
		w.Write(body)
	}

}
