package Service

type NotificationSmsRequest struct {
	Phone  string `json:"phone"`
	Method string
}
type NotificationSmsResponse struct {
	Message string `json:"result"`
	Code   int    `json:"code"`
}

type NotificationEmailRequest struct {
	Email  string `json:"email"`
	Method string
}
type NotificationEmailResponse struct {
	Result string `json:"result"`
}