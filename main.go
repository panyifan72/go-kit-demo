package main

import (
	"fmt"
	"go-kit-demo/Service"
	"go-kit-demo/Util"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	kitLog "github.com/go-kit/kit/log"
	transportHttp "github.com/go-kit/kit/transport/http"
	"github.com/golang/time/rate"
	"github.com/gorilla/mux"
)

//0 1 1 2 3 5 8
func feibo(n int) int {
	if n <= 0 {
		return 0
	}
	if n == 1 || n == 2 {
		return 1
	}

	return feibo(n-1) + feibo(n-2)
}
func forFeibo(n int) int {
	var arr []int
	arr = append(arr, 0)
	arr = append(arr, 1)
	arr = append(arr, 1)
	if n <= 2 {
		return arr[n]
	}

	i := 2
	for {
		i++
		arr = append(arr, arr[i-1]+arr[i-2])
		if i == n {
			break
		}
	}
	return arr[n]
}

func main() {

	//arr := []int{12, 345, 435, 34, 5536, 34, 252, 5, 3, 75}
	fmt.Println(forFeibo(10))
	os.Exit(1)
	userService := Service.UserService{}
	var logger kitLog.Logger
	{
		logger = kitLog.NewLogfmtLogger(os.Stdout)
		logger = kitLog.WithPrefix(logger, "mykit", "1.0")
		logger = kitLog.WithPrefix(logger, "time", kitLog.DefaultTimestampUTC) //加上前缀时间
		logger = kitLog.WithPrefix(logger, "caller", kitLog.DefaultCaller)     //加上前缀，日志输出时的文件和第几行代码

	}

	limit := rate.NewLimiter(1, 10)
	getEndPoints := Service.RateLimit(limit)((Service.UserServiceLogMiddleware(logger))(Service.GetUserEndPoint(userService))) //调用限流代码生成的中间件

	//getEndPoints := Service.GetUserEndPoint(userService)
	options := []transportHttp.ServerOption{
		transportHttp.ServerErrorEncoder(Service.UserErrorEncoder),
	}
	serverHander := transportHttp.NewServer(getEndPoints, Service.DecodeUserRequest, Service.EncodeUserRequest, options...)
	router := mux.NewRouter()
	router.Methods("GET", "DELETE").Path(`/user/{uid:\d+}`).Handler(serverHander)
	router.Methods("GET").Path(`/health`).HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Content-type", "application/json")
		writer.Write([]byte(`{"status":"ok"}`))
	})

	//-----
	//Service.SendSms("148332443")
	//-----

	errChan := make(chan error)
	go (func() {
		Util.RegisterServer()
		err := http.ListenAndServe(":7080", router)
		if err != nil {
			log.Println(err)
			errChan <- err
		}
	})()

	go (func() {
		sig_c := make(chan os.Signal)
		signal.Notify(sig_c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-sig_c)
	})()

	getErr := <-errChan
	Util.UnRegisterServer()
	log.Println(getErr)
}
