package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	hystrix_go "github.com/afex/hystrix-go/hystrix"
)

type Product struct {
	ID    int
	Title string
	Price int
}

func getProduct() (Product, error) {
	r := rand.Intn(10)
	if r < 6 { //模拟api卡顿和超时效果
		time.Sleep(time.Second * 4)
	}

	return Product{
		ID:    101,
		Title: "Golang从入门到精通",
		Price: 12,
	}, errors.New("aaa111")
}

func defaultProduct() (Product, error) {
	return Product{
		ID:    666,
		Title: "default product title",
		Price: 66,
	}, nil
}

func main() {
	rand.Seed(time.Now().UnixNano())

	getProdConfig := hystrix_go.CommandConfig{
		Timeout: 2000,
	}
	hystrix_go.ConfigureCommand("get_prod", getProdConfig)
	resultChan := make(chan Product, 1)
	for {

		goErr := hystrix_go.Do("get_prod", func() error { //使用hystrix来讲我们的操作封装成command
			p, goErrs := getProduct() //这里会随机延迟0-4秒
			if goErrs != nil {
				return goErrs
			}

			resultChan <- p

			return nil

		}, func(e error) error {
			getDetaultProduct, defaultError := defaultProduct()
			if defaultError != nil {
				return defaultError
			}

			resultChan <- getDetaultProduct

			return nil
		})

		select {
		case getPro := <-resultChan:
			fmt.Println(getPro)
		case err := <-goErr:
			fmt.Println("-----")
			fmt.Println(err)
		}

		time.Sleep(time.Second * 1)
	}
}
