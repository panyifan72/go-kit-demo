package Util

import (
	"fmt"
	consulApi "github.com/hashicorp/consul/api"
)

var ConsulClient *consulApi.Client

const REGISTRATION_ID = "test-user-8080"
const REGISTER_NAME = "test-kit-user"


func init() {
	config := consulApi.DefaultConfig()
	config.Address = "192.168.1.102:8500"
	client, err := consulApi.NewClient(config)
	if err != nil {
		fmt.Print(err)
	}

	ConsulClient = client
}

func RegisterServer() {
	registration := consulApi.AgentServiceRegistration{}
	registration.ID = REGISTRATION_ID
	registration.Name = REGISTER_NAME
	registration.Address = "192.168.1.102"
	registration.Port = 8080
	registration.Tags = []string{"first system"}

	check := consulApi.AgentServiceCheck{}
	check.Interval = "5s"
	check.HTTP = "http://192.168.1.102:8080/health"
	registration.Check = &check

	err := ConsulClient.Agent().ServiceRegister(&registration)
	if err != nil {
		fmt.Println(err)
	}
}

func UnRegisterServer() {
	ConsulClient.Agent().ServiceDeregister(REGISTRATION_ID)
}