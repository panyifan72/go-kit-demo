package Util

type Hystrix struct {
}

const (
	Timeout                = 2000
	MaxConcurrentRequests  = 5
	RequestVolumeThreshold = 3
	SleepWindow            = 10
	ErrorPercentThreshold  = 20

	NOTIF_CONFIG_NAME = "getuser"
)

