package Util

type UserError struct {
	Code    int
	Message string
}

func ShowError(code int, message string) error {
	return &UserError{Code: code, Message: message}
}

func (this *UserError) Error() string {
	return this.Message
}
