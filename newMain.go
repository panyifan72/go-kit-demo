package main

import (
	"fmt"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/consul"
	"github.com/go-kit/kit/sd/lb"
	transportHttp "github.com/go-kit/kit/transport/http"
	consulApi "github.com/hashicorp/consul/api"
	"io"
	"kit/Service"
	"net/url"
	"os"
	"context"
	"time"
)

func main() {
	getNotificationUrl()
}

func getNotificationUrl() string {
	config := consulApi.DefaultConfig()
	config.Address = "192.168.1.102:8500"
	apiClient, _ := consulApi.NewClient(config)
	consulClient := consul.NewClient(apiClient)

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stdout)
	}
	{
		tags := []string{"first system"}
		instancer := consul.NewInstancer(consulClient, logger, "test-kit-notification", tags, true)
		{
			factory := func(instance string) (endpoint.Endpoint, io.Closer, error) {
				tart, _ := url.Parse("http://" + instance)

				fmt.Println(tart)
				return transportHttp.NewClient("POST", tart, Service.SendSmsRequest, Service.SendSmsResponse).Endpoint(),nil,nil
			}
			endPointer := sd.NewEndpointer(instancer, factory, logger)
			clientEndpoints,_ := endPointer.Endpoints()
			fmt.Println("服务有",len(clientEndpoints),"条")
			//获取负载均衡的
			fmt.Println(clientEndpoints)
			mylb := lb.NewRoundRobin(endPointer)
			for {
				clientEndpoint,_ := mylb.Endpoint()
				//---------go send begin
				ctx := context.Background()
				res, err := clientEndpoint(ctx, Service.NotificationSmsRequest{Phone: "33534"})
				if err != nil {
					fmt.Println("nil")
					os.Exit(1)
				}

				smsResponse := res.(Service.NotificationSmsResponse)
				fmt.Println(smsResponse.Message)
				//---------go send end
				time.Sleep(3000)
			}


		}
	}

	return ""
}
