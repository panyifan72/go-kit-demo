package main

import "fmt"

func main() {
	dg(10)
}

func dg(a int) {
	if a > 0 {
		fmt.Printf("当前数据%d\n", a)
		a--
		dg(a)
		fmt.Printf("是这样的%d\n", a)
	}
	// for i:=0;i<a;i++ {

	// }
}

/*
searchArr := []int{11, 22, 34, 64, 75, 133, 174, 223, 387}
	searchValue := 223
	show2(searchArr, searchValue)
*/
func show2(arr []int, searchValue int) {
	var left, right, mid, arrLen int
	arrLen = len(arr)
	left = 0
	right = arrLen - 1
	for {
		mid = (left + right) / 2
		if left > right {
			fmt.Println("未找到")
			break
		}
		if arr[mid] < searchValue {
			left = mid + 1
		} else if arr[mid] > searchValue {
			right = mid - 1
		} else {
			fmt.Printf("找到了，下标为：%v \n", mid)
			break
		}
	}
}

/**
searchArr := []int{11, 22, 34, 64, 75, 133, 174, 223, 387}
	searchValue := 223
	result := show1(searchArr, searchValue, 0, len(searchArr))
	fmt.Println(result)
	fmt.Println("----")
*/
func show1(arr []int, searchValue, left, right int) int {
	var mid int
	mid = (left + right) / 2
	if left > right {
		return 0
	}

	if searchValue < arr[mid] { //比他小，往左查找
		show1(arr, searchValue, left, mid-1)
	} else if searchValue > arr[mid] { //比他大，需要往右查找
		show1(arr, searchValue, mid+1, right)
	} else {
		fmt.Printf("找到了，下标为：%v \n", mid)
	}

	return 0
}
