package main

import "fmt"

type numsStack struct {
	top   *numNode
	lengh int
}

type numNode struct {
	num int
	pre *numNode
}

type fhStack struct {
	top   *fhNode
	lenth int
}
type fhNode struct {
	fh  string
	pre *fhNode
}

func main() {
	//insert 5 7 3 2 6 3
	initNumStack := new(numsStack)
	initNumStack.pushNumStack(111)
	initNumStack.pushNumStack(222)
	initNumStack.pushNumStack(333)
	initNumStack.pushNumStack(444)
	fmt.Println(initNumStack.popNumsStack())
	fmt.Println(initNumStack.popNumsStack())
	fmt.Println(initNumStack.popNumsStack())
	//---------
	initFh := new(fhStack)
	initFh.pushFhStack("aaaa")
	initFh.pushFhStack("bbbb")
	initFh.pushFhStack("cccc")
}

func (this *numsStack) pushNumStack(value int) {
	preNode := new(numNode)
	preNode.num = value
	preNode.pre = this.top
	this.lengh++
	this.top = preNode
}

func (this *numsStack) popNumsStack() int {

	if this.lengh == 0 {
		return 0
	}
	temp := this.top
	this.top = temp.pre
	this.lengh--

	return temp.num
}

func (this *fhStack) pushFhStack(value string) {
	newValue := new(fhNode)
	newValue.fh = value
	newValue.pre = this.top
	this.top = newValue
	this.lenth++
}

func (this *fhStack) popFhStack() string {
	tempNode := this.top
	this.top = tempNode.pre
	this.lenth--

	return tempNode.fh
}
