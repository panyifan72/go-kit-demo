package main

import "fmt"

var result [][]int
var i int

func main() {
	track := make([]int, 0)
	ddd([]int{1, 2, 3}, track)
	// fmt.Println(result)
}

func ddd(nums, temp []int) {
	fmt.Println(temp)
	i++
	if i > 10 {
		return
	}
	if len(nums) == len(temp) {
		result = append(result, temp)
		fmt.Println("----")
		return
	} else {
		fmt.Println(9)
		for i := 0; i < len(nums); i++ {
			temp = append(temp, nums[i])
			ddd(nums, temp)
		}
	}
}

//result := permute([]int{1, 2, 3})
//	fmt.Println(result)
// track:=make([]int,0)
func permute(nums []int) [][]int {
	var tracks [][]int
	track := make([]int, 0)
	allTrack(nums, track, &tracks)
	return tracks
}

func allTrack(nums []int, track []int, tracks *[][]int) {
	// fmt.Println(track)
	if len(nums) == len(track) {
		temp := make([]int, len(track))
		copy(temp, track)
		(*tracks) = append((*tracks), temp)
		// fmt.Println("----",tracks)
		return
	}
	for i := 0; i < len(nums); i++ {
		if inList(nums[i], track) {
			continue
		}
		track = append(track, nums[i])
		allTrack(nums, track, tracks)
		track = track[:len(track)-1]
	}
}

func inList(key int, nums []int) bool {
	for _, v := range nums {
		if v == key {
			return true
		}
	}
	return false
}
