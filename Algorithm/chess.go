package main

import "fmt"

//实现棋盘
func main() {
	initChess()
}

type data struct {
	xCode, yCode, value int
}

type chessNode struct {
	total, weight, height int
	maps                  [][]int
	datas                 []data
}

func initChess() {
	var c chessNode
	c.total = 0
	c.weight = 10
	c.height = 10
	c.maps = make([][]int,10)
	for i:=range c.maps {
		c.maps[i] = make([]int,10)
	}
	c.maps[1][2] = 1
	c.maps[3][5] = 2

	var forArr [10][10]int

	for i := 0; i < c.weight; i++ {
		for j:=0;j<c.height;j++ {
			if c.maps[i][j] != 0 {
				fmt.Print(c.maps[i][j])
				fmt.Print("    ")
			} else {
				fmt.Print(forArr[i][j])
				fmt.Print("    ")
			}

		}
		fmt.Println("")
	}
}
