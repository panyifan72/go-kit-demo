package main

import "fmt"

var topSet []string //顶点数据
var numOfEdges int
var edges [][]int

//无向图的创建
func main() {

	//节点数量5
	numOfEdges = 5

	edges = make([][]int, 5)
	//-------
	initTopSet()
	initEdges(edges, numOfEdges)
	//-------
	setEdges(1, 3) //设置边
	printEdges()
}

//从第i个节点开始深度优先遍历
func traverse(i int) {

}

//打印出当前数据
func printEdges() {
	for j := 0; j < len(edges); j++ {
		fmt.Println(edges[j])
	}
}

//make数据
func initEdges(edges [][]int, numOfEdges int) {
	for i := 0; i < numOfEdges; i++ {
		edges[i] = make([]int, numOfEdges)
	}
}

func initTopSet() {
	topSet = []string{"A", "B", "C", "D", "E"}
}

func setEdges(x, y int) {
	edges[x][y] = 1
	edges[y][x] = 1
}
