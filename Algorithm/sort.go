package main

import (
	"fmt"
)

//var arr = []int{11, 22, 33, 44, 55, 66}
//fmt.Print(Rank(55, arr))
func Rank(key int, slice []int) int {
	lo := 0
	hi := len(slice)
	for lo <= hi {
		mid := lo + (hi-lo)/2
		if slice[mid] > key { //mid太大，则从小的走
			hi = mid - 1
		} else if slice[mid] < key {
			lo = mid + 1
		} else {
			return mid
		}
	}
	return -1

}

//选择排序
func ChoiseSort(array []int) {
	arrayLen := len(array)
	var minKey int
	for i := 0; i < arrayLen; i++ {
		minKey = i
		for j := i + 1; j < arrayLen; j++ {
			if array[minKey] < array[j] {
				minKey = j
			}
		}
		array[i], array[minKey] = array[minKey], array[i]
	}
	fmt.Println(array)
}

//插入排序
func InsertSort(array []int) {
	arrayLen := len(array)
	for i := 0; i < arrayLen; i++ {
		for j := i; j > 0; j-- {
			if array[j] < array[j-1] {
				array[j], array[j-1] = array[j-1], array[j]
			}
		}
	}
	fmt.Print(array)

}

//冒泡排序
func MaopaoSort(array []int) {
	arrayLen := len(array)
	for i := 0; i < arrayLen; i++ {
		for j := 0; j < arrayLen-1-i; j++ {
			if array[j] > array[j+1] {
				array[j], array[j+1] = array[j+1], array[j]
			}
		}
	}
	fmt.Print(array)
}

//array := [8]int{12, 42, 554, 633, 765, 23, 43, 45}
//希尔排序取N/2
func shellSort(array []int) {

	//12, 42, 554, 633, 765, 23, 43, 45分析
	//4间隔排序分为4个数组 12,765 42,23 554,43 633,45
	//排序结果为          12，23，43，45，765，42，554，633
	//2间隔排序分为两组    12,43,765,554  23,45,42,633
	//排序结果为			 12，43，42，554，23，765，633
	//1间隔排序           12,23,42,43,554,633,765
	arrayLen := len(array)
	//第一步循环为总共需要排序多少次
	//第二步循环为分开的n/2排序
	//第三步循环使用插入排序
	var i int

	for i = arrayLen / 2; i > 0; i /= 2 {
		for j := i; j < arrayLen; j++ { //为何用i，而不是用0，是因为在下一层的for循环中用k-i能够获取到最前面的数字，所以这里面可用i也可以用0
			for k := j; k >= i; k -= i {
				if array[k-1] > array[k] {
					array[k], array[k-i] = array[k-i], array[k]
				}
			}
		}
	}
	fmt.Println(array)
}

/*
-----------------
array := []int{12, 42, 554, 633, 765, 23, 43, 45, 98}
	result := mergeSort(array)
归并排序
*/
func mergeSort(array []int) []int {
	if len(array) < 2 {
		return array
	}

	mid := len(array) / 2
	left := array[0:mid]
	right := array[mid:]

	return goMmergeSort(mergeSort(left), mergeSort(right))
}

func goMmergeSort(arraya []int, arrayb []int) []int {
	ai := 0
	bi := 0
	var newArray []int

	for ai < len(arraya) && bi < len(arrayb) {
		if arraya[ai] <= arrayb[bi] {
			newArray = append(newArray, arraya[ai])
			ai++
		} else {
			newArray = append(newArray, arrayb[bi])
			bi++
		}
	}

	if ai < len(arraya) {
		for ai < len(arraya) {
			newArray = append(newArray, arraya[ai])
			ai++
		}
	}

	if bi < len(arrayb) {
		for bi < len(arrayb) {
			newArray = append(newArray, arrayb[bi])
			bi++
		}
	}

	return newArray
}

//归并排序结束---------------

//快速排序--------
func quickSort(arr []int) []int {
	//获取切分点
	return goQuickSort(arr, 0, len(arr)-1)
}
func goQuickSort(arr []int, left, right int) []int {

	if left < right {
		partitionIndex := partition(arr, left, right)
		goQuickSort(arr, left, partitionIndex-1)
		goQuickSort(arr, partitionIndex+1, right)
	}

	return arr
}

//第一种实现
/// 6 1 3 7 9 11 4 5 10 8
/// 6 1 3 5 9 11 4 7 10 8
/// 6 1 3 5 4 11 9 7 10 8
/// 4 1 3 5 6 11 9 7 10 8
func onePartition(arr []int, left, right int) int {
	//待实现
	return 0
}

//第二种实现
// 6 1 3 7 9 11 4 5 10 8
// 6 1 3 4 9 11 7 5 10 8
// 6 1 3 4 5 11 7 9 10 8
// 5 1 3 4 6           11 7 9 10 8
// 4 1 3    5 6
// 3 1    4    5 6
// 1 3    4    5 6

//arr[i] = 4 arr[index] = 7
//arr[i] = 5 arr[index] = 9
//基于第二种实现
func partition(arr []int, left, right int) int {
	pivot := left      //取第一个为基准值
	index := pivot + 1 //循环起始位置
	for i := index; i <= right; i++ {
		if arr[i] < arr[pivot] { //如果i里面的元素小于基准值
			arr[i], arr[index] = arr[index], arr[i]
			index++
		}
	}
	arr[pivot], arr[index-1] = arr[index-1], arr[pivot]

	return index - 1
}

//快速排序结束--------

//堆排序开始——-------
/*
array := []int{45, 42, 554, 633, 765, 23, 43, 98, 12}
					12
		42						23
	45     765				554     43
   98  633
*/
func heapSort(arr []int) {
	arrLen := len(arr)
	buildHeap(arr, arrLen)
	//[12 42 23 45 765 554 43 98 633]
	//现在状态为
	/*
		     	765
		633				554
	98		42		23		43
   43 45   12 ~
	*/
	for i := arrLen-1; i >= 0; i-- {
		arr[i],arr[0] = arr[0],arr[i]
		arrLen = arrLen-1
		heapify(arr,0,arrLen)
	}

fmt.Println(arr)
}
func buildHeap(arr []int, arrLen int) {
	for i := arrLen / 2; i >= 0; i-- {
		heapify(arr,i,arrLen)
	}
}

func heapify(arr []int, i int, arrLen int) {
	iLeftNode := 2*i+1
	irightNode := 2*i+2
	largest := i

	if iLeftNode < arrLen && arr[iLeftNode] > arr[largest] {
		largest = iLeftNode
	}
	if irightNode < arrLen && arr[irightNode] > arr[largest] {
		largest = irightNode
	}

	if largest != i {
		arr[i],arr[largest] = arr[largest],arr[i]
		heapify(arr,largest,arrLen)
	}

}

//堆排序结束----------

func main() {
	array := []int{45, 42, 554, 633, 765, 23, 43, 98, 12}
	heapSort(array)
	//fmt.Println(result)
}
