package main

import (
	"fmt"
)

type Tree interface {
	CreateNode(v int) //创建第一个节点
	Put(v int)        //新增修改
	Find(v int)       //获取
	FindList()        //遍历树
	Delete(v int)     //删除节点
}

type Node struct {
	LeftChild  *Node
	RightChild *Node
	value      int
}

//遍历查找
//先序遍历
//fmt.Println(node.value,"-----")
//	node.LeftChild.FindList()
//	node.RightChild.FindList()

//中序遍历
//	node.LeftChild.FindList()
//fmt.Println(node.value,"-----")
//	node.RightChild.FindList()

//后序遍历
//	node.LeftChild.FindList()
//	node.RightChild.FindList()
//fmt.Println(node.value,"-----")

func (node *Node) FindList() {
	if node == nil {
		return
	}

	node.LeftChild.FindList()
	fmt.Println(node.value)
	node.RightChild.FindList()
}

func getleftNodeRightSecondMinNode(node *Node) (*Node, bool) { //找出右树下倒数第二大的元素
	var secondMinNode *Node

	if node.RightChild.LeftChild == nil { //如果当前节点的左节点为空，则直接返回当前节点
		return node.RightChild, false
	}

	node = node.RightChild
	for {
		if node.LeftChild.LeftChild == nil {
			secondMinNode = node
			break
		}

		node = node.LeftChild
	}

	return secondMinNode, true
}

//
//func (node *Node)getLeftMaxNode() *Node{
//
//}

//func (node *Node)findNodeExists()

func (node *Node) Delete(v int) bool {
	fmt.Println("delete debug begin")
	var deleteResult bool
	//第一个查找是否存在
	if node.value == v { //如果删除的第一个节点即为要删除的节点，则另外处理
		fmt.Println("delete first node")
		getSecondMinRightNode, hasChild := getleftNodeRightSecondMinNode(node)
		if hasChild == true { //说明下面还有各种孩子，要使用当前的下一个节点
			node.value = getSecondMinRightNode.LeftChild.value
			getSecondMinRightNode.LeftChild = nil
		} else { //说明地下没有孩子了，直接使用当前节点
			node.value = getSecondMinRightNode.value
			node.RightChild = nil
		}
	} else {
		for {
			fmt.Println(node.value)
			if v < node.value { //从左树开始找
				if node.LeftChild.value == v { //如果左树的孩子的值等于要删除的元素
					deleteResult = true
					fmt.Println("find it in left")
					if node.LeftChild.LeftChild == nil && node.LeftChild.RightChild == nil { //说明没有孩子了，直接设置为空
						node.LeftChild = nil
						break
					} else if node.LeftChild.LeftChild == nil && node.LeftChild.RightChild != nil { //如果左树为空，则直接复制为右树
						node.LeftChild = node.LeftChild.RightChild
						break
					} else if node.LeftChild.LeftChild != nil && node.LeftChild.RightChild == nil { //和上面相反
						node.LeftChild = node.LeftChild.LeftChild
						break
					} else { //如果两个都不为空
						//中序遍历右树第一个节点，把第一个节点放上来
						getSecondMinRightNode, hasChild := getleftNodeRightSecondMinNode(node.LeftChild)
						fmt.Println(getSecondMinRightNode, hasChild)
						if hasChild == true { //说明下面还有各种孩子，要使用当前的下一个节点
							node.LeftChild.value = getSecondMinRightNode.LeftChild.value
							getSecondMinRightNode.LeftChild = nil
						} else { //说明地下没有孩子了，直接使用当前节点
							node.LeftChild.value = getSecondMinRightNode.value
							node.LeftChild.RightChild = nil
						}
						break
					}
				} else {
					node = node.LeftChild
				}
			}

			if v > node.value {
				if node.RightChild.value == v { //如果右树的孩子的值等于要删除的元素
					deleteResult = true
					if node.RightChild.LeftChild == nil && node.RightChild.RightChild == nil { //说明没有孩子了，直接设置为空
						node.RightChild = nil
						break
					} else if node.RightChild.LeftChild == nil && node.RightChild.RightChild != nil { //如果左树为空，则直接复制为右树
						node.RightChild = node.RightChild.RightChild
						break
					} else if node.RightChild.RightChild == nil && node.RightChild.LeftChild != nil { //和上面相反
						node.RightChild = node.RightChild.LeftChild
						break
					} else { //如果两个都不为空
						//中序遍历右树第一个节点，把第一个节点放上来
						getSecondMinRightNode, hasChild := getleftNodeRightSecondMinNode(node.RightChild)
						fmt.Println(getSecondMinRightNode, hasChild)

						if hasChild == true { //说明下面还有各种孩子，要使用当前的下一个节点
							node.RightChild.value = getSecondMinRightNode.LeftChild.value
							getSecondMinRightNode.LeftChild = nil
						} else {//说明地下没有孩子了，直接使用当前节点
							node.RightChild.value = getSecondMinRightNode.value
							node.RightChild = getSecondMinRightNode
						}
						break
					}
				} else {
					node = node.RightChild
				}
			}

			if node.LeftChild == nil && node.RightChild == nil {
				break
			}
		}
	}
	//记录删除元素的上一个指针
	//var getDeleteNodesPreNode *Node

	fmt.Println("delete debug end")
	return deleteResult
}

func (node *Node) Put(newNode *Node) bool {
	fmt.Println("insert debug begin")
	putResult := false
	for {
		if newNode.value < node.value { // 比他小，则需要往左查找
			if node.LeftChild == nil { //说明右边没有值，则要进行处理
				node.LeftChild = newNode
				putResult = true
				break
			}
			node = node.LeftChild
		} else if newNode.value > node.value { //比他大，则需要往右查找

			if node.RightChild == nil { //说明右边没有值，则要进行处理
				node.RightChild = newNode
				putResult = true
				break
			}
			node = node.RightChild
		} else {
			break
		}
	}

	fmt.Println("insert debug end")
	return putResult
}

func (node *Node) Find(v int) *Node {
	var findValue *Node
	for {
		if v == node.value {
			findValue = node
		} else if v < node.value { //要查找的值比value小，则进行左子树查找
			node = node.LeftChild
			if node.LeftChild == nil { //说明查找完毕，没有查找到
				break
			}
		} else {
			node = node.RightChild
			if node.RightChild == nil { //说明查找完毕，没有查找到
				break
			}
		}
	}

	return findValue
}

//新建第一个节点
func (node *Node) CreateNode(v int) {
	node.value = v
}

func main() {
	var node Node
	//模拟数据开始
	node.CreateNode(17) //新建第一个节点
	//模拟一棵树
	node.LeftChild = &Node{value: 10}
	node.RightChild = &Node{value: 20}
	node.LeftChild.LeftChild = &Node{value: 8}
	node.LeftChild.RightChild = &Node{value: 13}
	node.RightChild.LeftChild = &Node{value: 19}
	node.RightChild.RightChild = &Node{value: 25}
	//模拟数据结束
	//测试创建
	newNodeData := Node{value: 26}
	node.Put(&newNodeData) //创建节点
	otherNode := Node{value: 9}
	node.Put(&otherNode) //创建节点
	otherNode2 := Node{value: 18}
	node.Put(&otherNode2) //创建节点
	//otherNode3 := Node{value: 23}
	//node.Put(&otherNode3) //创建节点
	node.FindList()
	fmt.Println("--")
	//删除
	//fmt.Println(node.Delete(9))
	//fmt.Println(node.Delete(26))
	node.Delete(20)
	node.FindList()
}
