package main

import "fmt"

type treeStruct struct {
	value int
	left  *treeStruct
	right *treeStruct
}

func main() {
	goList := []int{13, 7, 8, 3, 29, 6, 1}

	var sd treeStruct
	maxValue := sd.createTree(goList)
	fmt.Println("最大值是 : ", maxValue)
	sd.bl()
}

func (this *treeStruct) createTree(goList []int) int {
	var firstNode, secondNode treeStruct
	if len(goList) != 1 {
		goList = goChoiseSort(goList)
		firstNode.value = goList[0]
		secondNode.value = goList[1]

		this.value = goList[0] + goList[1]
		this.left = &firstNode
		this.right = &secondNode
		goList = append(goList[:0], goList[1:]...)
		goList = append(goList[:0], goList[1:]...)
		goList = append(goList, this.value)
		this.createTree(goList)

	}

	return goList[0]
}

func (this *treeStruct) bl() {
	fmt.Println(this.value)
	if this.left != nil {
		this.left.bl()
	}
	if this.right != nil {
		this.right.bl()
	}
}

//选择排序
func goChoiseSort(array []int) []int {
	arrayLen := len(array)
	var minKey int
	for i := 0; i < arrayLen; i++ {
		minKey = i
		for j := i + 1; j < arrayLen; j++ {
			if array[minKey] > array[j] {
				minKey = j
			}
		}
		array[i], array[minKey] = array[minKey], array[i]
	}

	return array
}
