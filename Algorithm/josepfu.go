package main

import (
	"fmt"
)

//实现环形链表
type node struct {
	value int
	next  *node
}

func (this *node) insert(value int) {
	node := new(node)
	node.value = value
	node.next = this
	if this.next == nil { //说明加入是第一个节点
		this.value = node.value
		this.next = this
		return
	}

	headNode := this
	for {
		if this.next == headNode { //说明已经循环到最后
			//需要进行特殊处理
			this.next = node
			break
		}
		this = this.next
	}
}

func (this *node) delete(value int) bool {
	headNode := this
	if this.value == value && this.next == this { //首节点是要删除的节点
		this.next = nil
		return true
	}

	var result bool

	for {
		if this.next.value == value { //如果当前节点的下一个节点是要删除的值
			this.next = this.next.next
		}

		if headNode == this.next {
			result = false
			break
		}

		this = this.next
	}

	return result
}

func (this *node) showList() {
	if this.next == nil {
		return
	}

	headList := this
	for {
		fmt.Print(this.next)
		fmt.Print(this.value)
		fmt.Println("")
		if this.next == headList {
			break
		}

		this = this.next
	}
}

func main() {
	head := new(node)
	head.insert(1)
	head.insert(2)
	head.insert(3)
	head.insert(5)
	head.insert(40)
	head.insert(25)
	head.delete(2)
	head.showList()
	fmt.Println("-----")
}
