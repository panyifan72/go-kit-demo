package main

import "fmt"

//插值查找
func main() {
	findArr := []int{11, 22, 33, 44, 44, 55, 66, 77, 88, 99}
	result := findMid2(findArr, 0, len(findArr)-1, 44)
	fmt.Print(result)
	//findArr := []int{11, 22, 33, 44, 55, 66, 77, 88, 99}
	//result := findMid(findArr, 0, len(findArr)-1, 99)
	//fmt.Print(result)
}

func findMid2(arr []int, beginLeft, beginRight, findVal int) []int {
	var mid int
	var result []int
	mid = (beginLeft + beginRight) / 2

	if beginLeft > beginRight {
		return result
	}

	if arr[mid] < findVal {
		return findMid2(arr, mid+1, beginRight, findVal)
	} else if arr[mid] > findVal {
		return findMid2(arr, beginLeft, mid-1, findVal)
	} else { //找到了
		result = append(result, mid)
		var tempMid int
		tempMid = mid - 1
		for {
			if tempMid < 0 || arr[tempMid] != findVal {
				break
			}
			result = append(result, tempMid)
			tempMid = tempMid - 1
		}

		tempMid = mid + 1
		for {
			if tempMid > len(arr)-1 || arr[tempMid] != findVal {
				break
			}
			result = append(result, tempMid)
			tempMid = tempMid + 1
		}

		return result
	}
}

func findMid(arr []int, beginLeft, beginRight, findVal int) int {
	var mid int
	mid = (beginLeft + beginRight) / 2
	if arr[mid] == findVal {
		return mid
	}

	if beginLeft > beginRight {
		return -1
	}

	if arr[mid] < findVal {
		return findMid(arr, mid+1, beginRight, findVal)
	} else if arr[mid] > findVal {
		return findMid(arr, beginLeft, mid-1, findVal)
	}

	return -1
}

func findMid3(arr []int, beginLeft, beginRight, findVal int) int {
	//mid = left + (right-left) * (findVal-arr[left])/(arr[right]-arr[left])
	if beginLeft > beginRight || findVal < arr[0] || findVal > arr[len(arr)-1] {
		return -1
	}

	mid := beginLeft + (beginRight-beginLeft)*(findVal-arr[beginLeft])/(arr[beginRight]-arr[beginLeft])
	if arr[mid] < findVal {
		return findMid3(arr, mid+1, beginRight, findVal)
	} else if arr[mid] > findVal {
		return findMid3(arr, beginLeft, mid-1, findVal)
	} else {
		return mid
	}
}
