package main

import "fmt"

func initMaps() [][]int {
	maps := make([][]int, 8)
	for i := 0; i < len(maps); i++ {
		maps[i] = make([]int, 8)
	}

	//先围一个圈
	for i := 0; i < len(maps); i++ {
		maps[0][i] = 1
		maps[7][i] = 1
		maps[i][0] = 1
		maps[i][7] = 1
	}

	maps[3][1] = 1
	maps[3][2] = 1
	return maps
}
func main() {
	maps := initMaps()
	beginPositionx := 1
	beginPositiony := 1
	endPointX := 6
	endPointY := 6
	findWay(maps, beginPositionx, beginPositiony, endPointX, endPointY)

	for i := 0; i < len(maps); i++ {
		for j := 0; j < len(maps[i]); j++ {
			fmt.Print(maps[i][j])
			fmt.Print("    ")
		}
		fmt.Println("")
	}
}

func findWay(maps [][]int, beginPositionx, beginPositiony, endPointsX, endPointsY int) bool {
	if maps[endPointsX][endPointsY] == 2 { //说明已经到达终点了，可以返回
		return true
	} else {
		if 0 == maps[beginPositionx][beginPositiony] {
			//假设这条路为通路，则把他设置为2
			maps[beginPositionx][beginPositiony] = 2
			if findWay(maps, beginPositionx+1, beginPositiony, endPointsX, endPointsY) { //往下走
				return true
			} else if findWay(maps, beginPositionx, beginPositiony+1, endPointsX, endPointsY) { //往右走
				return true
			} else if findWay(maps, beginPositionx-1, beginPositiony, endPointsX, endPointsY){//往左走
				return true
			} else if findWay(maps, beginPositionx, beginPositiony+1, endPointsX, endPointsY){//往上走
				return true
			} else {
				maps[beginPositionx][beginPositiony] = 3
				return false
			}
		} else {
			return false
		}
	}
}
