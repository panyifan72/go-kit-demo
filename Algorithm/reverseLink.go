package main

import "fmt"

type ListNode struct {
	data int
	next *ListNode
}

func main() {
	var head = new(ListNode)
	createNode(head, 10)
	printNodeList(head)
	fmt.Println("------")
	d := reverse(head)
	printNodeList(d)
}

func createNode(node *ListNode, max int) {
	cur := node
	for i := 1; i < max; i++ {
		cur.next = &ListNode{}
		cur.next.data = i
		cur = cur.next
	}
}
func printNodeList(head *ListNode) {
	for {
		fmt.Println(head)
		if head.next == nil {
			break
		}

		head = head.next
	}
}

func reverse(head *ListNode) *ListNode{
	var pre *ListNode
	var next *ListNode
	for head != nil {
		next = head.next
		head.next = pre
		pre = head
		head = next
	}

	return pre
}
